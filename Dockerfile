FROM debian:stable-slim

RUN apt-get update -q
RUN apt-get install --no-install-recommends -qy \
    texlive-full \
    biber \
    make \
    chktex \
    git
    
RUN rm -rf /var/lib/apt/lists/*
